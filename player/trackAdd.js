const Discord = require('discord.js');
module.exports = (client, message, queue, track) => {
  const exampleEmbed = new Discord.MessageEmbed()
    .setColor('#00A551')
    .setAuthor(`Artiest: ${track.author}`)
	  .setTitle(`${client.emotes.music} - ${track.title}`)
	  .setDescription(`**${track.requestedBy.username}** heeft een nummer toegevoegd aan **${message.member.voice.channel.name}**`)
	  .setURL('https://minecraft.scouting.nl/')
	  .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
	  .setTimestamp();
  message.channel.send(exampleEmbed)
};
