const Discord = require('discord.js')
module.exports = (client, message, track) => {
        const EmbedClear = new Discord.MessageEmbed()
            .setColor('##00A551')
            .setTitle(`De muziek is gestopt`)
            .setDescription(`Er stond geen nieuwe muziek klaar in de wachtrij. Wil je nieuwe muziek toevoegen? typ dan:
               **/play [nummer]**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();

        message.channel.send(EmbedClear);
};