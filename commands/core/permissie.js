const Discord = require('discord.js');
var DotJson = require('dot-json');

function trusted(x) {
  var DotJson = require('dot-json');
  var database = new DotJson('../blocked.json');
  var trustedMembers =  database.get('trusted');
  for (y = 0; y < trustedMembers.length; y++) {
    if (trustedMembers[y] == x) {return true}
  } return false
}

module.exports = {
    name: 'permissie',
    aliases: [],
    category: 'Mods',
    utilisation: '{prefix}permissie [@member], Dit ontneemt de rechten van een gebruiker om alle commandos behalve skip en stop te gebruiken van de muziekbots',

    execute(client, message, args, track) {
      if(trusted(message.author.id) == true) {
        var bericht = message.content.split(" ")
        var id = bericht[1] // TAG <@!0000000>
        var newId = id.replace('@', '').replace('!', '').replace('>', '').replace('<', '');
        var geblokkerdePersonen = new DotJson('../blocked.json');
        var blackList =  geblokkerdePersonen.get('blocked')
        const EmbedVerbannen = new Discord.MessageEmbed()
          .setColor('#00BFFF')
          .setTitle(`Deze speler mag vanaf nu geen liedjes meer toevoegen`)
          .setDescription(bericht[1] + ' is verbannen van het gebruik van de muziekbots')
          .setURL('https://minecraft.scouting.nl/')
          .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
          .setTimestamp();
        const EmbedPardon = new Discord.MessageEmbed()
          .setColor('#00BFFF')
          .setTitle(`Deze speler mag weer liedjes toevoegen`)
          .setDescription(bericht[1] + ' mag weer liedjes afspelen op de muziekbots')
          .setURL('https://minecraft.scouting.nl/')
          .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
          .setTimestamp();
        const EmbedAlVerbannen = new Discord.MessageEmbed()
          .setColor('#e71837')
          .setTitle(`Deze speler is al verbannen van de muziekbots`)
          .setDescription(bericht[1] + ' was al verbannen op de muziekbots')
          .setURL('https://minecraft.scouting.nl/')
          .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
          .setTimestamp();

        function checkVrijlaten(x) {
          var arrayId = geblokkerdePersonen.get('blocked');
          for (y = 0; y < arrayId.length; y++) {
            if (x == arrayId[y]) {
              blackList.splice(y, 1)
              message.channel.send(EmbedPardon);
              var nieuweBlacklist = new DotJson('../blocked.json'); //
              nieuweBlacklist.set('blocked', blackList).save();
              return true
            }
          } return false
        }
        if (checkVrijlaten(newId) == true) {return}
        function checkId(x) {
          var arrayId = geblokkerdePersonen.get('blocked');
          for (y = 0; y < arrayId.length; y++) {
            if (x == arrayId[y]) {return true}
          }
          return false
        }
        if (checkId(newId) == true) {return message.channel.send(EmbedAlVerbannen)}
        blackList.push(newId)  // [..., ..., TAG]
        message.channel.send(EmbedVerbannen);

        var nieuweBlacklist = new DotJson('../blocked.json'); //
        nieuweBlacklist.set('blocked', blackList).save();
      } else {
      message.channel.send({
                      embed: {
                          color: '#e71837',
                          author: { name: 'Geen rechten' },
                          timestamp: new Date(),
                          description: `Je hebt de rechten niet om **/permissie** uit te mogen voeren`,
                      },
                  })};
}};
