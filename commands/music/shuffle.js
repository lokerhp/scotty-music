const Discord = require('discord.js');
module.exports = {
    name: 'shuffle',
    aliases: ['sh'],
    category: 'Music',
    utilisation: '{prefix}shuffle',

    execute(client, message) {
        const EmbedNotInChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
            .setTitle(`Je zit niet in een spraakkanaal`)
            .setDescription(`Om de muziek te shuffelen moet je in het volgende spraakkanaal zitten: **${message.guild.me.voice.channel}**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (!message.member.voice.channel) return message.channel.send(EmbedNotInChannel);


        const EmbedNotInSameChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
            .setTitle(`Je zit niet in het goede spraakkanaal`)
            .setDescription(`Om de muziek te shuffelen moet je in het volgende spraakkanaal zitten: **${message.guild.me.voice.channel}**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (message.guild.me.voice.channel && message.member.voice.channel.id !== message.guild.me.voice.channel.id) return message.channel.send(EmbedNotInSameChannel);


        const EmbedNoSongPlaying = new Discord.MessageEmbed()
            .setColor('#fc9303')
            .setTitle(`Er speelt geen muziek`)
            .setDescription(`Er speelt nog geen muziek. Voeg liedjes toe met
                **/play [nummer]**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (!client.player.getQueue(message)) return message.channel.send(EmbedNoSongPlaying);

        client.player.shuffle(message);
        const EmbedShuffled = new Discord.MessageEmbed()
            .setColor('#fc9303')
            .setTitle(`Wachtrij door elkaar geklutst`)
            .setDescription(`**${client.player.getQueue(message).tracks.length}** liedjes gehusseld`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        return message.channel.send(EmbedShuffled);
    },
};