const Discord = require('discord.js')
module.exports = {
    name: 'nowplaying',
    aliases: ['np'],
    category: 'Music',
    utilisation: '{prefix}nowplaying',

    execute(client, message) {
        const EmbedNotInChannel = new Discord.MessageEmbed()
                    .setColor('#e71837')
                	.setTitle(`Je zit niet in een spraakkanaal`)
                	.setDescription(`Als je dit commando uit wil voeren moet je **${message.guild.me.voice.channel}** joinen`)
                	.setURL('https://minecraft.scouting.nl/')
                	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                	.setTimestamp();
        if (!message.member.voice.channel) return message.channel.send(EmbedNotInChannel);


        const EmbedNoSongInQueue = new Discord.MessageEmbed()
            .setColor('#fc9303')
            .setTitle(`De wachtrij is leeg!`)
            .setDescription(`Er zitten nog geen liedjes in de wachtrij. Voeg liedjes toe met
                **/play [nummer]**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (!client.player.getQueue(message)) return message.channel.send(EmbedNoSongInQueue);


        const EmbedNotInSameChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
            .setTitle(`Je zit niet in het goede spraakkanaal`)
            .setDescription(`Als je dit commando uit wil voeren moet je in het volgende spraakkanaal zitten:
               **${message.guild.voice.channel.name}**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (message.guild.me.voice.channel && message.member.voice.channel.id !== message.guild.me.voice.channel.id) return message.channel.send(EmbedNotInSameChannel);


        const track = client.player.nowPlaying(message);
        const filters = [];

        Object.keys(client.player.getQueue(message).filters).forEach((filterName) => client.player.getQueue(message).filters[filterName]) ? filters.push(filterName) : false;

        message.channel.send({
            embed: {
                color: '#00A551',
                author: { name: track.title },
                fields: [
                    { name: 'Artiest', value: track.author, inline: true },
                    { name: 'Toegevoegd door', value: track.requestedBy.username, inline: true },
                    { name: 'Speellijst?', value: track.fromPlaylist ? 'Ja' : 'Nee', inline: true },

                    { name: 'Weergaven', value: track.views, inline: true },
                    { name: 'Duratie', value: track.duration, inline: true },
                    { name: 'Actieve filters', value: filters.length + '/' + client.filters.length, inline: true },

                    { name: 'Volume', value: client.player.getQueue(message).volume, inline: true },
                    { name: 'Herhaal-modus', value: client.player.getQueue(message).repeatMode ? 'Ja' : 'Nee', inline: true },
                    { name: 'Gepauseerd?', value: client.player.getQueue(message).paused ? 'Ja' : 'Nee', inline: true },

                    { name: 'Voortgang', value: client.player.createProgressBar(message, { timecodes: true }), inline: true }
                ],
                thumbnail: { url: track.thumbnail },
                timestamp: new Date(),
            },
        });
    },
};